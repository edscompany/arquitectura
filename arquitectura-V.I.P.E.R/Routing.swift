//
//  Routing.swift
//  arquitectura-V.I.P.E.R
//
//  Created by edgar rivera on 10/05/16.
//  Copyright © 2016 Ed Company. All rights reserved.
//

import Foundation

import UIKit

class Routing {
    //build objects
    let vc:TableViewController = TableViewController()
    let presenter:Presenter = Presenter()
    let interactor:Interactor = Interactor()
    
    var navigationController:UINavigationController?
    
    init(){
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.routing = self
        interactor.presenter = presenter
        navigationController = UINavigationController(rootViewController: vc)
        
    }
    
  
    func openAddView() {
        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        
        let addVC:ViewController = storyBoard.instantiateViewControllerWithIdentifier("ViewController") as! ViewController
        addVC.presenter = self.presenter
        
        vc.presentViewController(addVC, animated: true, completion: nil)
        
        
    }
 
    
    
}