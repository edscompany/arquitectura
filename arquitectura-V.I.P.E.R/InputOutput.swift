//
//  InputOutput.swift
//  arquitectura-V.I.P.E.R
//
//  Created by edgar rivera on 11/05/16.
//  Copyright © 2016 Ed Company. All rights reserved.
//

import Foundation

protocol InteractorProtocolInput {
    
    func addNewPersonWithData(nombre:String, apellido:String)
}

protocol InteractorProtocolOutput {
    
    func updateObjects(objects:[String])
    
}