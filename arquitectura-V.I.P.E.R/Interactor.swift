//
//  Interactor.swift
//  arquitectura-V.I.P.E.R
//
//  Created by edgar rivera on 10/05/16.
//  Copyright © 2016 Ed Company. All rights reserved.
//

import Foundation

class Interactor: InteractorProtocolInput {
    
    var presenter:Presenter?
    var database:DataBase?
    
    init(){
        database = DataBase()
        
    }
    
    func addNewPersonWithData(nombre:String, apellido:String) {
        
         if (!nombre.isEmpty && !apellido.isEmpty) {
            print("No Vacio")
            let persona = Persona()
            persona.nombre = nombre
            persona.apellido = apellido
            
            if let personas = database?.personas{
                database?.personas?.append(persona)
            }else{
                database?.personas = [Persona]()
                database?.personas?.append(persona)
            }
           self.updateList()
        }
    }
    
    func updateList(){
        var arrayPersonas = [String]()
        
        for persona in (database!.personas)! {
            arrayPersonas.append(persona.nombre! + " " + persona.apellido!)
        }
        presenter?.updateObjects(arrayPersonas)
    }
}