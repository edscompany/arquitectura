//
//  ViewController.swift
//  arquitectura-V.I.P.E.R
//
//  Created by edgar rivera on 11/05/16.
//  Copyright © 2016 Ed Company. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITextFieldDelegate {

    
    @IBOutlet weak var nombre: UITextField!
    
    @IBOutlet weak var apellido: UITextField!
    var presenter:Presenter?
    
    @IBAction func add(sender: UIButton) {
        presenter?.addNewObjectWithData(nombre: self.nombre.text!, apellido: self.apellido.text!)
        
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    @IBAction func cancel(sender: UIButton) {
        self.presentedViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
}
